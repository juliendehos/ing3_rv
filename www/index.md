EIL3Info, Réalité virtuelle
===========================

* * * * *

Organisation
------------

-   28h de séances encadrées
-   2h d'examen sur table (document autorisé : une feuille de notes A4 manuscrite
    comportant votre nom; pas de photocopie)

séance | sujet | documents |
--- | --- | --- |
CM1 | introduction à la réalité virtuelle | [notes de cours](EIL3Info_RV_CM1.pdf) |
CM2 | développement d'une application de réalité virtuelle | [notes de cours](EIL3Info_RV_CM2.pdf) |
TP+projet | développement d'une application de réalité virtuelle | [sujet](EIL3Info_RV_TP.pdf), [bibliothèque herve](https://github.com/juliendehos/herve) |

* * * * *

[Retour à la page d'accueil](../../index.html)

Dernière mise à jour : 2016-02-24

