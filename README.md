# réalité virtuelle

EIL3 Info

## volume horaire

- 2*2h CM
- 4*3h TP
- 4*3h projet
- 2h EX

## planning

- CM1 2h Introduction à la réalité virtuelle
- CM2 2h Développement d'une application de réalité virtuelle

- TP1 3h Application de base, interaction
- TP2 3h Navigation, matrice, shader
- TP3 3h Objet 3D, texture
- TP4 3h Animation, son 3D, video

- TP5 3h Projet
- TP6 3h Projet
- TP7 3h Projet
- TP8 3h Projet

